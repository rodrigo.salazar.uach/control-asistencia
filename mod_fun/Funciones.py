'''MODULO DE FUNCIONES, METODOS Y CLASES DEL SISTEMA
    El bloque de funciones, es un bloque modular que contiene toda las clases y funciones de nuestro sistema, ademas de ser uno de 
  los paquetes más importantes. Estan funciones estan disponibles para ser llamadas desde el modulo "principal" y en ella encontramos 
  las siguientes funciones y clases:

  - Funcion list_Personas
  - Funcion codificar
  - Clase facial
  - Funcion generar_rec
  - Funcion registrar_ingresos
  - Funcion hablar
  - Funcion testeo 
  - Funcion presionar_teclado.

  '''

#Librerias 
from cv2 import cv2
import face_recognition as fr
import pyttsx3
from datetime import datetime
import curses
import multiprocessing as mp
import os

#Numero de procesadores de nuestra computadora, utilizando la libreria multiprocessing
print("Numero de procesadores: ", mp.cpu_count())


#Cargar imagenes y nombres
def list_personas(lista_empleados, mis_imagenes, nombre_empleados, ruta):
    '''LISTA DE PERSONAS
    Funcion encargada de cargar las imagenes de la ruta empleados como lista. Ademas es capaz de solo entregar 
    el nombre sin su extension (o tipo de archivo), haciendo uso de la libreria os mediante el metodo 
    splitext'''
    for nombre in lista_empleados:

        #método una imagen que se carga desde el archivo especificado
        imagen_actual = cv2.imread(f'{ruta}/{nombre}')
        #Se almacena la imagen como lista
        mis_imagenes.append(imagen_actual)
        #Se almacena solo el texto (dejando de lado el tipo de archivo) como lista
        nombre_empleados.append(os.path.splitext(nombre)[0])

    return lista_empleados, mis_imagenes, nombre_empleados, ruta

#funcion de codificacion en imagenes
def codificar(imagenes):

    ''' CODIFICACION DE LAS IMAGENES
    Función que se utiliza para devolver una lista de imagenes codificadas de la ruta (o directorio) 
    empleados, el loop for permitira que todas las imagenes del directorio sean codificadas. Para codificar las imagenes 
    para luego realizar la comparacion, es necesario utilizar el metodo face_encodings de la clase face_recognition, 
    en donde dada una imagen, devuelve la codificación facial de 128 dimensiones para cada rostro de la imagen.
    '''
    #creacion de una lista nueva
    lista_codificada = [] 

    for imagen in imagenes:
        
        imagen = cv2.cvtColor(imagen, cv2.COLOR_BGR2RGB) 

        #Agregar a la lista
        codificado = fr.face_encodings(imagen)[0]
        
        #codificar
        lista_codificada.append (codificado)
    
    return lista_codificada

#Clase face_recognition
class facial:
    '''METODOS E INSTANCIAS DE LIBRERIA FACE_RECOGNITION
    Clase para llamar metodos de la libreria Face_recognition, encargadas de encontrar la localizacion facial,
    la codificacion de las de la imagenes, la busquedas de las coincidencias y distancias entre dos focos de imagenes o videos. '''

    def __init__ (self, cara):
        self.cara = cara

    def cara_localizada (self, img):
        ''' Este metodo localiza el rostro de una img y lo devuelve una matriz de cuadros delimitadores de rostros humanos 
        en una imagen.
        Parametros:

        img (img):Una imagen (como una matriz numpy)

        Devuelve una lista de tuplas de ubicaciones de caras encontradas en orden css (arriba, derecha, abajo, izquierda
        '''

        #reconocer cara en captura
        self.cara_l = fr.face_locations(img)
        #print("Se ha llamado al metodo localizacion de cara")

        return self.cara_l
    
    def cara_codificada (self, imgs, imgs_n):
        '''Dada una imagen, devuelve la codificación facial de 128 dimensiones para cada rostro de la imagen.
        Parametros:

        - imgs(face_image ):La imagen que contiene una o mas caras
        - imgs_n(known_face_locations): Los cuadros delimitadores de cada cara si ya los conoce. 

        Devuelve una lista de codificaciones de caras de 128 dimensiones (una para cada cara en la imagen). 
        '''
        
        #Codificar cara capturada
        self.cara_c = fr.face_encodings (imgs, imgs_n)
        #print("Se ha llamado al metodo de codificacion de cara")

        return self.cara_c

    def coincidencias (self, coinc, comp):
        '''Compara una lista de codificaciones faciales con una codificación candidata para ver si coinciden. 
        Parametros:

        -coinc (face_encodings): lista de codificaciones faciales para comparar.
        -comp (face_to_compare): Una codificacion facial para comparar.
        
        Devuelve Una lista de valores Verdadero/Falso que indica qué codificaciones faciales conocidas coinciden con 
        la codificación facial para verificar.
        '''

        #Comparacion en distancia de caras
        self.cara_co = fr.compare_faces(coinc, comp)
        #print("Se ha llamado al metodo de comparacion de cara")

        return self.cara_co

    def distancias (self, dist, codif):

        '''Dada una lista de codificaciones faciales, compárelas con una codificación facial conocida y obtenga 
        una distancia euclidiana para cada cara de comparación. La distancia te dice qué tan similares son las caras. 
        Parametros:

        dist (face_encodings): Lista de codificaciones faciales para comparar.
        codif (face_to_compare): Una codificacion facial para comparar.

        Devuelve un ndarray numpy con la distancia para cada cara en el mismo orden que la matriz de 'caras'.
        '''

        #comparacion en distancias de caras
        self.cara_d = fr.face_distance(dist, codif)
        #print("Se ha llamado al metodo de comparacion de distancias en caras")

        return self.cara_d


#generar rectangulos para la localizacion de la cara
def generar_rec(ubicacion, img, emp):

    '''RECTANGULOS LOCALIZADOS EN LA IMAGEN DE LA WEB
    Esta funcion depende de la instancia de cara_localizada (ubicada en la clase facial), dado que al localizar un rostro en una imagen 
    se desplegara un rectangulo que hara seguimiento de dicho rostro ocupando el metodo cv2 de openCV. Para localizar dicho 
    rostro es necesario tener activa la imagen o camara, que en este caso se llamada mediante la varible img. La imagen tendra 
    el nombre del empleado asociado, es por esto que se llamo al metodo putText de openCV, para agregar el nombre del empleado
    debajo del cuadrado generado en la localizacion de la cara.'''

    y1, x2, y2, x1 = ubicacion
    y1, x2, y2, x1 = y1*4,x2*4,y2*4,x1*4
    cv2.rectangle(img,(x1,y1),(x2,y2),(0,255,0),2)
    cv2.rectangle(img,(x1,y2-35),(x2,y2),(0,255,0),cv2.FILLED)

    #generación de texto en la imagen
    cv2.putText(img,emp,(x1+6,y2-6),cv2.FONT_HERSHEY_COMPLEX,1,(255,255,255),2)

    return img, emp, ubicacion

#Registro de datos de los empleados
def registrar_ingresos(persona):

    '''REGISTRO DE PERSONAS DETECTADAS EN LA WEB-CAM EN UN ARCHIVO .CSV
    Funcion que registra el nombre de la personas en un archivo registro.csv, integrando su nombre y la hora de llegada del empleado. 
    Ademas se incluyo un if para no adicionar los nombres ya inscrito en el registro, esto se logro introduciendo una variable nombres_registro
    que cumple la función de avisar si el nombre ya se encuentra en el registro. 
    '''
    with open('modata/registro.csv', 'r+') as f:
        lista_datos = f.readlines()
        nombres_registro = []
        for linea in lista_datos:
            ingreso = linea.split(',')
            nombres_registro.append(ingreso[0])

        #configuracion del reloj
        if persona not in nombres_registro:
            ahora = datetime.now()
            string_ahora = ahora.strftime('%H:%M:%S')
            f.writelines(f'\n{persona}, {string_ahora}')

    return persona

#funcion para que el asistente pueda ser escuchado
def hablar (mensaje):

    '''ACTIVACIÓN DEL ASISTENTE DE VOZ, CUANDO UN ROSTRO CONOCIDO ES DETECTADO EN LA WEB-CAM
    Funcion encargada de activar una asistente de voz cuando es este detecta en una camara la imagen de un empleado. 
    Se selecciono una voz en español latino y es llamada mediante el metodo setProperty(), realizando la accion de hablar 
    mediante el metodo say().'''

    voice_id = 'spanish-latin-am'

    #Encender el motor de pyttsx3
    engine = pyttsx3.init()
    engine.setProperty('voice', voice_id)

    #Pronunciar mensaje
    engine.say(mensaje)
    engine.runAndWait()

    return mensaje


def testeo(stdscr):
    stdscr.nodelay(True)
    try:
        return stdscr.getkey()
    except:
        return None

def presionar_teclado(tecla):
    '''BOTON DE INACTIVIDAD 
    Activa la funcionalidad de presionar una tecla para interrumpir cualquier procedimiento desde la terminal.'''

    tecla_entrada = curses.wrapper(testeo)
    while tecla_entrada is not None:
        if tecla == tecla_entrada:
            return True
        tecla_entrada = curses.wrapper(testeo)
    
    return False




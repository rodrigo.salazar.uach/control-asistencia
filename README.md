## DESCRIPCIÓN COMPLETA DEL PROYECTO: CONTROL Y REGISTRO DE ASISTENCIA MEDIANTE RECONOCIMIENTO FACIAL

### INFORMACIÓN DEL PROYECTO:

Presentación del proyecto: [Link de Documentación](https://rodrigo.salazar.uach.gitlab.io/control-asistencia/)

Documentación completa y detallada del proyecto: [Link de Documentación](https://rodrigo.salazar.uach.gitlab.io/control-asistencia/documentacion.html)

### CONCEPTOS PREVIOS:

Como recomendación es importante que usted pueda reforzar diferentes disciplinas y conceptos que son fundamentales para entender en mayor medida y detalle el funcionamiento del proyecto:

Documentación sobre Visión Artificial: [Link de Documentación](https://docs.python.org/es/3/)

Documentación de Python (Información General): [Link de Documentación](https://docs.python.org/es/3/)

Documentación sobre Computación Científica con Python: [Link de Documentación](https://phuijse.github.io/PythonBook/README.html)

Documentación de Machine Learning: [Link de Documentación](https://phuijse.github.io/MachineLearningBook/README.html)

### ENCARGADOS Y CURSO:

- Curso: Herramientas Informáticas en Electricidad y Electrónica - ELEL329 
- Desarrollador del Proyecto: Rodrigo Salazar Belmar
- Académico Encargado: Daniel Luhr Sierra

Universidad austral de Chile (UACH): [Pagina Oficial](https://www.uach.cl/)

### ANUNCIOS Y AGRADECIMIENTOS:

Para apoyar la iniciativa de este proyecto e informar de cualquier clase de error (o critica constructiva), por favor no dudes en enviarme un correo a la dirección rodrigo.salazar.uach@gmail.com. Muchas gracias por tu atención y espero que te haya servido este material como fuente de inspiración para tu desarrollo en general.

<div align="center"> <img src="https://i.postimg.cc/wTFqnGL5/python-skillshare-1.webp" alt="python-skillshare-1.webp" width="700"/> </div>
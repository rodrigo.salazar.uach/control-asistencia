'''MODULO PRINCIPAL ENCARGADO DE LLAMAR LAS FUNCIONES E INSTANCIAS DEL SISTEMA
    Bloque principal encargardo de activar la visualizacion y las acciones totales del sistema. Sin embargo desde este modulo
   se llaman los metodologias de los demas modulos de paquetes, especificamente las funciones, clases y metodos.Loop infinito 
   que tiene como objetivos encontrar coincidencias y distancias mediante una base de datos que incluyen antecedentes previos 
   de los empleados. 
   Ademas esta integrado por un loop "while" principal, que funciona como intermediario entre las interacciones de los empleados 
   y la camara, en donde se desea encontrar coincidencias y distancias desde un directorio que almacena imagenes que contienen los 
   rostros de los empleados. Se compone principalmente por la asignacion de diferentes funciones y metodos relacionados con la 
   libreria OpenCV, para activar procesos como la activacion constante de la web-cam, como tambien la codificacion de ciertas escenas 
   tomadas por la camara. Ademas se hace llamado a constantes metodos y clases del paquete funciones, como es el caso de la clase 
   facial, la funciones codificar, list_empleados, hablar. etc.
   '''

import os
from cv2 import cv2 
#from mod_fun import Funciones as fu
from mod_fun import Funciones as fu
import numpy as np

#buscando eficiencia en el codigo
import cProfile
import pstats
#import snakeviz
#import multiprocessing as mp

#pool = mp.Pool(mp.cpu_count())

#Crear una base de datos de los Empleados
ruta = 'Empleados'

mis_imagenes = []
nombre_empleados = []
lista_empleados = os.listdir(ruta)

#tomar una captura de la camara web
captura = cv2.VideoCapture (0)

#Estado del loop while
estado = True

#clase facial del modulo funciones
clase_cara = fu.facial("cara")

#print(lista_empleados)

#loop encargado de cargar la lista de imagenes y de nombres
fu.list_personas(lista_empleados,mis_imagenes, nombre_empleados,ruta)

#print(nombre_empleados)

#Se ha codificado las imagenes llamando la funcion codificar desde el packete funciones
lista_emp_cod = fu.codificar(mis_imagenes)
#print(len(lista_emp_cod))



while estado:

    #leer imagen de la camara
    exito, imagen = captura.read()

    #Cambiar el tamaño de una imagen 
    imagenS = cv2.resize(imagen, (0,0), None, 0.25, 0.25)
    
    #Conversion de la imagen en el color "BGR2RGB"
    imagenS = cv2.cvtColor(imagenS, cv2.COLOR_BGR2RGB)

    #aplicando el metodo cara localizada a imagenS
    cara_cap = clase_cara.cara_localizada(imagenS)

    #Aplicando el metodo cara codificada a imagenS y cara_cap
    cara_cap_cod = clase_cara.cara_codificada(imagenS, cara_cap)

    #Buscar coincidencias mediante los metodos de clase cara
    for caracodif, caraubic in zip(cara_cap_cod, cara_cap):
        coincidencias = clase_cara.coincidencias(lista_emp_cod, caracodif)
        distancias = clase_cara.distancias(lista_emp_cod, caracodif)
        #print(distancias)
        #print(coincidencias)

        #llamando al metodo argmin de numpy, devuelve los valores minimos de distancias
        indice_coincidencia = np.argmin(distancias)

        #mostrar si hay coincidencias utilizando la distancias
        if distancias[indice_coincidencia] > 0.6:
            print("No coincide con ninguno de nuestros empleados")

        #mostrar las coincidencias 
        if coincidencias[indice_coincidencia]:
            nombre = nombre_empleados[indice_coincidencia].upper()
            #print(nombre)

            fu.generar_rec(caraubic, imagen, nombre)
            #fu.hablar(f'Acaba de llegar {nombre}')
            #pool.apply_async(fu.hablar, args = (f'Acaba de llegar {nombre}'))
            fu.registrar_ingresos(nombre)
            

    #Apretar 'q' para apagar el programa
    apagar = fu.presionar_teclado("q")
    if apagar:
        estado = False
    
    #mostrar la cara de la persona
    cv2.imshow('Webcam', imagen)


    #mantener ventana abierta
    cv2.waitKey(1)

def main ():
    '''EVALUACIÓN DEL RENDIMIENTO MEDIANTE CPROFILE
    Funcion que nos permite observar los tiempos y retardos en el proceso de ejecucion de las funciones, clases o metodos.
    En este caso se aplico Cprofile a la mayoria de las funciones del modulo funciones, para poder evaluar una probabilidad 
    de reducir los tiempos de ejecucion del codigo'''

    cProfile.run('fu.generar_rec(caraubic, imagen, nombre)')
    cProfile.run('fu.registrar_ingresos(nombre)')
    cProfile.run('fu.codificar(mis_imagenes)')
    cProfile.run('clase_cara.cara_localizada(imagenS)')
    cProfile.run('clase_cara.cara_codificada(imagenS, cara_cap)')
    cProfile.run('clase_cara.coincidencias(lista_emp_cod, caracodif)')
    cProfile.run('clase_cara.distancias(lista_emp_cod, caracodif)')
    cProfile.run('apagar = fu.presionar_teclado("q")')
    cProfile.run('fu.list_personas(lista_empleados,mis_imagenes, nombre_empleados,ruta)')
    cProfile.run('fu.hablar(f"Acaba de llegar {nombre}")')

if __name__=='__main__':
    
    profiler = cProfile.Profile()
    profiler.enable()
    main ()
    profiler.disable()
    stats = pstats.Stats(profiler).sort_stats('cumtime')
    # stats.dump_stats('/modata/perfilador.csv')
    stats.strip_dirs()
    stats.print_stats()




